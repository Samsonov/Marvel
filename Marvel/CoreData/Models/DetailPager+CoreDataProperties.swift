//
//  DetailPager+CoreDataProperties.swift
//  
//
//  Created by Pavel on 02.02.17.
//
//

import Foundation
import CoreData


extension DetailPager {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DetailPager> {
        return NSFetchRequest<DetailPager>(entityName: "DetailPager");
    }

    @NSManaged public var htmlString: String?
    @NSManaged public var id: Int32

}
