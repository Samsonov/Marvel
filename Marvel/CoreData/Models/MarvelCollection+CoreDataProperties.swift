//
//  MarvelCollection+CoreDataProperties.swift
//  
//
//  Created by Pavel on 02.02.17.
//
//

import Foundation
import CoreData


extension MarvelCollection {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MarvelCollection> {
        return NSFetchRequest<MarvelCollection>(entityName: "MarvelCollection");
    }

    @NSManaged public var index: Int32
    @NSManaged public var thumbnailPath: String?

}
