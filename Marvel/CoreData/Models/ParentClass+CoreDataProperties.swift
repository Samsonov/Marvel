//
//  ParentClass+CoreDataProperties.swift
//  
//
//  Created by Pavel on 02.02.17.
//
//

import Foundation
import CoreData


extension ParentClass {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ParentClass> {
        return NSFetchRequest<ParentClass>(entityName: "ParentClass");
    }


}
