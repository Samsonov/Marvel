//
//  ComicPagerCharacter+CoreDataProperties.swift
//  
//
//  Created by Pavel on 02.02.17.
//
//

import Foundation
import CoreData


extension ComicPagerCharacter {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ComicPagerCharacter> {
        return NSFetchRequest<ComicPagerCharacter>(entityName: "ComicPagerCharacter");
    }

    @NSManaged public var name: String?
    @NSManaged public var resourceURI: String?
    @NSManaged public var comic: ComicPager?

}
