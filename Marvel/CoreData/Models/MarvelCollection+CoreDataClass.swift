//
//  MarvelCollection+CoreDataClass.swift
//  
//
//  Created by Pavel on 02.02.17.
//
//

import Foundation
import CoreData

@objc(MarvelCollection)
public class MarvelCollection: ParentClass {

}

extension MarvelCollection: Equatable {
    public static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.index == rhs.index
    }
}

extension MarvelCollection: Comparable {
    public static func <(lhs: Self, rhs: Self) -> Bool {
        return lhs.index < rhs.index
    }
    
    public static func <=(lhs: Self, rhs: Self) -> Bool {
        return lhs.index <= rhs.index
    }
    
    public static func >=(lhs: Self, rhs: Self) -> Bool {
        return lhs.index >= rhs.index
    }
    
    public static func >(lhs: Self, rhs: Self) -> Bool {
        return lhs.index > rhs.index
    }
}
