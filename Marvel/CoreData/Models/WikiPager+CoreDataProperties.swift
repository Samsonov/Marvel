//
//  WikiPager+CoreDataProperties.swift
//  
//
//  Created by Pavel on 02.02.17.
//
//

import Foundation
import CoreData


extension WikiPager {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WikiPager> {
        return NSFetchRequest<WikiPager>(entityName: "WikiPager");
    }

    @NSManaged public var descriptionComic: String?
    @NSManaged public var index: Int32
    @NSManaged public var name: String?
    @NSManaged public var thumbnailPath: String?

}
