//
//  ComicPager+CoreDataProperties.swift
//  
//
//  Created by Pavel on 02.02.17.
//
//

import Foundation
import CoreData

extension ComicPager {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ComicPager> {
        return NSFetchRequest<ComicPager>(entityName: "ComicPager");
    }

    @NSManaged public var index: Int32
    @NSManaged public var characters: Set<ComicPagerCharacter>

}

// MARK: Generated accessors for characters
extension ComicPager {

    @objc(addCharactersObject:)
    @NSManaged public func addToCharacters(_ value: ComicPagerCharacter)

    @objc(removeCharactersObject:)
    @NSManaged public func removeFromCharacters(_ value: ComicPagerCharacter)

    @objc(addCharacters:)
    @NSManaged public func addToCharacters(_ values: NSSet)

    @objc(removeCharacters:)
    @NSManaged public func removeFromCharacters(_ values: NSSet)

}
