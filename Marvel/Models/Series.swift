//
//  Series.swift
//  Marvel
//
//  Created by Pavel on 20.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct Series : Mappable {
    
    var id:          Int?
    var title:       String?
    var description: String?
    var resourceURI: String?
    var urls:        [URLType]?
    var startYear:   Int?
    var endYear:     Int?
    var rating:      String?
    var type:        String?
    var modified:    String?
    var thumbnail:   CharacterImage?
    var creators:    CreatorsList?
    var characters:  CharactersList?
    var stories:     StoriesList?
    var comics:      ComicsList?
    var events:      EventsList?
    var next:        SeriesSummary?
    var previous:    SeriesSummary?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        title       <- map["title"]
        description <- map["description"]
        resourceURI <- map["resourceURI"]
        urls        <- map["urls"]
        startYear   <- map["startYear"]
        endYear     <- map["endYear"]
        rating      <- map["rating"]
        type        <- map["type"]
        modified    <- map["modified"]
        thumbnail   <- map["thumbnail"]
        creators    <- map["creators"]
        characters  <- map["characters"]
        stories     <- map["stories"]
        comics      <- map["comics"]
        events      <- map["events"]
        next        <- map["next"]
        previous    <- map["previous"]
    }
}

