//
//  DataContainer.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct DataContainer<T : Mappable> : Mappable {
    var offset:  Int?
    var limit:   Int?
    var total:   Int?
    var count:   Int?
    var results: [T]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        offset  <- map["offset"]
        limit   <- map["limit"]
        total   <- map["total"]
        count   <- map["count"]
        results <- map["results"]
    }
}
