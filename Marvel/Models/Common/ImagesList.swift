//
//  ImagesList.swift
//  Marvel
//
//  Created by Pavel on 22.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct ImagesList: Mappable {
    var description: String?
    var events:      ItemsCharactersType?
    var id:          Int?
    var modified:    String?
    var name:        String?
    var resourceURI: String?
    var series:      ItemsCharactersType?
    var stories:     StoriesResource?
    var thumbnail:   ImagesResource?
    var urls:        URLType?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        description <- map["description"]
        events      <- map["events"]
        id          <- map["id"]
        modified    <- map["modified"]
        name        <- map["name"]
        resourceURI <- map["resourceURI"]
        series      <- map["series"]
        stories     <- map["stories"]
        thumbnail   <- map["thumbnail"]
        urls        <- map["urls"]
    }
}

struct ImagesResource: Mappable {
    var extensionImage: String?
    var path:           String?
    
    init() {}
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        extensionImage <- map["extension"]
        path           <- map["path"]
    }
}
