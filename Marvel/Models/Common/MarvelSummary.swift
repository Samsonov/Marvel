//
//  MarvelSummary.swift
//  Marvel
//
//  Created by Pavel on 30.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation

public struct MarvelSummary {
    var index:       Int32
    var resourceURI: String
    
    init(index: Int32, resourceURI: String) {
        self.index       = index
        self.resourceURI = resourceURI
    }
}
