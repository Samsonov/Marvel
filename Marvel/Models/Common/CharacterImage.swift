//
//  CharacterImage.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

protocol ImageOrientationSize {
    var description: String { get }
}

enum ImageStandardSize : CustomStringConvertible, ImageOrientationSize {
    case small
    case medium
    case large
    case xlarge
    case fantastic
    case amazing
    
    var description: String {
        switch self {
        case .small:     return "standard_small"
        case .medium:    return "standard_medium"
        case .large:     return "standard_large"
        case .xlarge:    return "standard_xlarge"
        case .fantastic: return "standard_fantastic"
        case .amazing:   return "standard_amazing"
        }
    }
}

enum ImagePortraitSize : CustomStringConvertible, ImageOrientationSize {
    case small
    case medium
    case xlarge
    case fantastic
    case uncanny
    case incredible
    
    var description: String {
        switch self {
        case .small:      return "portrait_small"
        case .medium:     return "portrait_medium"
        case .xlarge:     return "portrait_large"
        case .fantastic:  return "portrait_xlarge"
        case .uncanny:    return "portrait_fantastic"
        case .incredible: return "portrait_amazing"
        }
    }
}

struct CharacterImage: Mappable {
    var path: String?
    var fileExtension: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        path          <- map["path"]
        fileExtension <- map["extension"]
    }
    
    func fullPathImage(_ size: ImageOrientationSize) -> String? {
        if let imagePath = self.path, let imageExtension = self.fileExtension {
            return imagePath + "/" + size.description + "." + imageExtension
        }
        return nil
    }
}




