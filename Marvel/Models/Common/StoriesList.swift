//
//  StoriesList.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct StoriesList: Mappable {
    var available:     Int?
    var collectionURI: String?
    var items:        [StoriesResource]?
    var returned:      Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        available     <- map["available"]
        collectionURI <- map["collectionURI"]
        items         <- map["items"]
        returned      <- map["returned"]
    }
}

struct StoriesResource: Mappable {
    var resourceURI: String?
    var name:        String?
    var type:        String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        resourceURI <- map["resourceURI"]
        name        <- map["name"]
        type        <- map["type"]
    }
}
