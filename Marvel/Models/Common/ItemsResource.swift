//
//  ItemsResource.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct ItemsResource: Mappable {
    var resourceURI: String?
    var name:        String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        resourceURI <- map["resourceURI"]
        name        <- map["name"]
    }
}
