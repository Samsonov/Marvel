//
//  ImagesData.swift
//  Marvel
//
//  Created by Pavel on 22.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct ImagesData: Mappable {
    var count:   Int?
    var limit:   Int?
    var offset:  Int?
    var results: [ImagesList]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        count   <- map["count"]
        limit   <- map["limit"]
        offset  <- map["offset"]
        results <- map["results"]
    }
}

