//
//  EventsList.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct EventsList: Mappable {
    var available:     Int?
    var collectionURI: String?
    var items :        [EventsResource]?
    var returned:      Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        available     <- map["available"]
        returned      <- map["returned"]
        collectionURI <- map["collectionURI"]
        items         <- map["items"]
    }
}

struct EventsResource : Mappable {
    var resourceURI: String?
    var name:        String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        resourceURI <- map["resourceURI"]
        name        <- map["name"]
    }
}

