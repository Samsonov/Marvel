//
//  CreatorsList.swift
//  Marvel
//
//  Created by Pavel on 21.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct CreatorsList : Mappable {
    
    var available:     Int?
    var collectionURI: String?
    var items:         [ItemsType]?
    var returned:      Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        available     <- map["available"]
        collectionURI <- map["collectionURI"]
        items         <- map["items"]
        returned      <- map["returned"]
    }
}
