//
//  ItemsType.swift
//  Marvel
//
//  Created by Pavel on 20.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct ItemsType: Mappable {
    
    var resourceURI: String?
    var name:        String?
    var role:        String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        resourceURI <- map["resourceURI"]
        name        <- map["name"]
        role        <- map["role"]
    }
}
