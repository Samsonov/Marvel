//
//  URLType.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct URLType: Mappable {
    var urlType: String?
    var url:     String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        urlType <- map["type"]
        url     <- map["url"]
    }
}

