//
//  Character.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

class TransformUrls: TransformType {
    typealias Object = String
    typealias JSON   = AnyObject
    
    let key: String
    
    init(key: String) {
        self.key = key
    }
    
    func transformFromJSON(_ value: Any?) -> String? {
        guard let array = value as? Array<[String:String]> else { return nil }
        
        for elem in array {
            if elem["type"] == key {
                return elem["url"]
            }
        }
        return nil
    }
    
    func transformToJSON(_ value: String?) -> AnyObject? {
        return nil
    }
}

struct Character : Mappable {

    var id:          Int?
    var name:        String?
    var description: String?
    var modified:    String?
    var thumbnail:   CharacterImage?
    var resourceURI: String?
    var comics:      ComicsList?
    var series:      SeriesList?
    var stories:     StoriesList?
    var events:      EventsList?
    var detailLink:  String?
    var wikiLink:    String?
    var comicLink:   String?
    
    init() {}
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {

        id          <- map["id"]
        name        <- map["name"]
        description <- map["description"]
        modified    <- map["modified"]
        thumbnail   <- map["thumbnail"]
        resourceURI <- map["resourceURI"]
        comics      <- map["comics"]
        series      <- map["series"]
        stories     <- map["stories"]
        events      <- map["events"]
        detailLink  <- (map["urls"], TransformUrls(key: "detail"))
        wikiLink    <- (map["urls"], TransformUrls(key: "wiki"))
        comicLink   <- (map["urls"], TransformUrls(key: "comiclink"))
    }
}
