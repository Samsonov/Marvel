//
//  DataWrapper.swift
//  Marvel
//
//  Created by Pavel Samsonov on 06.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct DataWrapper<T : Mappable> : Mappable {
    var code            : Int?
    var status          : String?
    var copyright       : String?
    var attributionText : String?
    var attributionHTML : String?
    var etag            : String?
    var data            : T?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        code            <- map["code"]
        status          <- map["status"]
        copyright       <- map["copyright"]
        attributionText <- map["attributionText"]
        attributionHTML <- map["attributionHTML"]
        etag            <- map["etag"]
        data            <- map["data"]
    }
}
