//
//  Images.swift
//  Marvel
//
//  Created by Pavel on 22.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import ObjectMapper

struct Images : Mappable {
    var attributionHTML: String?
    var attributionText: String?
    var code:            Int?
    var copyright:       String?
    var data:            ImagesData?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        attributionHTML <- map["attributionHTML"]
        attributionText <- map["attributionText"]
        code            <- map["code"]
        copyright       <- map["copyright"]
        data            <- map["data"]
    }
}
