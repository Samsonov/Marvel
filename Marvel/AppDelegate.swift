//
//  AppDelegate.swift
//  Marvel
//
//  Created by Pavel Samsonov on 02.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
        [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupControllers()
        setupReachability()
        
        return true
    }
    
    open func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.sharedInstance.saveContext()
    }
}

internal extension AppDelegate {
    func setupControllers() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let marvelCollectionVC = MarvelCollectionVC(nibName: "MarvelCollectionVC", bundle: nil)
        let aboutVC            = AboutVC(nibName: "AboutVC", bundle: nil)
        
        let marvelNC = UINavigationController(rootViewController: marvelCollectionVC)
        let aboutNC  = UINavigationController(rootViewController: aboutVC)
        
        marvelNC.navigationBar.backgroundColor = UIColor.white
        marvelNC.navigationBar.barTintColor    = UIColor.white
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.white
        }
        
        let tabBarController                    = UITabBarController()
        tabBarController.tabBar.backgroundColor = UIColor.white
        tabBarController.tabBar.barTintColor    = UIColor.white
        tabBarController.viewControllers        = [marvelNC, aboutNC]
        
        let marvelItem = tabBarController.tabBar.items?[0]
        let aboutItem  = tabBarController.tabBar.items?[1]
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.black], for: .normal)
        
        marvelItem?.title = "Marvel"
        marvelItem?.image = UIImage(named: "marvelTabBarImage")?.withRenderingMode(.alwaysOriginal)
        
        aboutItem?.title = "About"
        aboutItem?.image = UIImage(named: "aboutTabBarImage")?.withRenderingMode(.alwaysOriginal)
        
        self.window?.rootViewController = tabBarController
        self.window?.makeKeyAndVisible()
    }
    
    func setupReachability() {
        ReachabilityManager.sharedInstance.startNetworkListening()
    }
}


















