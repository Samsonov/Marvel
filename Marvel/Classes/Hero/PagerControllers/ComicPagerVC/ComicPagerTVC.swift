//
//  ComicPagerTVC.swift
//  Marvel
//
//  Created by Pavel on 21.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import CoreData

private let comicReuseIdentifier = "ComicCell"

class ComicPagerTVC: UITableViewController, IndicatorInfoProvider {
    var characterId:      Int32?
    // QUESTION: массивы и словари лучше объявлять с выставленным типом
    var dataCharacter     = [Series]()
    var dataImages        = [String]()
    var dataCharacters    = [ItemsCharactersType]()
    var activityIndicator = ActivityIndicatorVC()
    var notFound          = false
    var loadMoreStatus    = true
    var imagePath         = ImagesResource()
    let entityName        = "ComicPager"

    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = ComicItemCell.nibInstance()
        self.tableView.register(nib, forCellReuseIdentifier: comicReuseIdentifier)
        activityIndicator.showActivityIndicator(tableView)
        
        if connectToInternet {
            ApiConnection.sharedInstance.getSeriesWithId(characterId: self.characterId!,
                                                   completionHandler: { [weak self] data in
                guard let sself = self else { return }
                guard let character = data else { return }
                
                sself.dataCharacter = character
                
                if sself.dataCharacter.count > 0 {
                    var number: Int = (sself.dataCharacter.first?.characters?.items?.count)!
                    
                    sself.dataCharacter.first?.characters?.items?.forEach({ (thumbnail) in
                        ApiConnection.sharedInstance.getSeriesWithURL(url: thumbnail.resourceURI!, completionHandler: { [weak self] data in
                            guard let sself = self else { return }
                            guard let path = data else { return }
                            
                            sself.imagePath = path
                            let resultPath = sself.imagePath.path! + "." + sself.imagePath.extensionImage!
                            self?.dataImages.append(resultPath)
                            number -= 1
                            
                            if number == 0 {
                                sself.mainReload()
                                sself.loadToCoreData(index: Int32(sself.characterId!),
                                                 itemsType: (sself.dataCharacter.first?.characters?.items)!,
                                                imagesPath: sself.dataImages)
                            }
                        })
                    })
                } else {
                    sself.notFound = true
                    sself.mainReload()
                }
            }, failure: { error in
                if let err = error {
                    print(err)
                }
            })
        }
        else {
            do {
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
                let results = try context.fetch(request)
                
                // QUESTION: зачем тут делается проверка на непустой массив
                // Нужно избегать методов такой длины и такой вложенности
                if results.count > 0 {
                    for result in results {
                        if let indexPath = (result as! ComicPager).value(forKey: "index") as? Int32 {
                            if characterId == indexPath {
                                let characters = (result as! ComicPager).value(forKey: "characters") as! Set<ComicPagerCharacter>
                                
                                for comic in characters {
                                    var charactersType = ItemsCharactersType()
                                    charactersType.name = comic.name
                                    charactersType.resourceURI = comic.resourceURI
                                    
                                    dataCharacters.append(charactersType)
                                }
                                notFound = false
                            } else {
                                notFound = true
                            }
                        }
                        if !notFound {
                            break
                        }
                    }
                    activityIndicator.hideProgressView()
                    tableView.reloadData()
                }
            } catch {
                print("Error when requesting ComicPager")
            }
        }
        tableView.rowHeight = 103.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Comic")
    }
    
    static func storyboardInstance() -> ComicPagerTVC {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ComicPagerTVC
    }

    // MARK: - TableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = dataCharacter.first?.characters?.items?.count {
            return count
        } else if dataCharacters.count > 0 {
            return dataCharacters.count
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: comicReuseIdentifier, for: indexPath) as! ComicItemCell
        
        if notFound {
            cell.comicNameLabel.text = "Not found"
        } else if connectToInternet {
            guard var comic = dataCharacter.first?.characters?.items?[indexPath.row] else { return cell }
            comic.resourceURI = dataImages[indexPath.row]
            cell.comic = comic
        } else {
            cell.comic = dataCharacters[indexPath.row]
        }
        
        return cell
    }
}

extension ComicPagerTVC {
    fileprivate func mainReload() {
        DispatchQueue.main.async { [weak self] in
            guard let sself = self else { return }
            
            sself.activityIndicator.hideProgressView()
            sself.tableView.reloadData()
        }
    }
}

// MARK: CoreData
extension ComicPagerTVC {
    fileprivate func loadToCoreData(index: Int32, itemsType: [ItemsCharactersType], imagesPath: [String]) -> Void {
        var existIndex = false
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results {
                    if let indexPath = (result as! ComicPager).value(forKey: "index") as? Int32 {
                        if index == indexPath {
                            existIndex = false
                            break
                        } else {
                            existIndex = true
                        }
                    }
                }
            } else {
                existIndex = true
            }
        } catch {
            print("Not found")
        }
        
        if existIndex {
            let comic = ComicPager(context: context)
            
            // QUESTION: можно проще
            for (i, item) in itemsType.enumerated() {
                
            }
            for i in 0 ..< itemsType.count {
                let comicPager = ComicPagerCharacter(context: context)
                comicPager.name = itemsType[i].name
                comicPager.resourceURI = imagesPath[i]
                
                comic.index = index
                comic.addToCharacters(comicPager)
            }
            do {
                try context.save()
            } catch {
                print("Failed to save")
            }
        }
    }
    
    fileprivate func getComics() -> Void {
        do {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for item in results {
                    // QUESTION: очень часто и очень много где применяется force cast, это плохо
                    let index      = (item as! ComicPager).value(forKey: "index") as! Int32
                    let characters = (item as! ComicPager).value(forKey: "characters") as! Set<ComicPagerCharacter>
                    
                    print(index, characters.count)
                }
            }
        } catch {
            print("Not found")
        }
    }
}






















