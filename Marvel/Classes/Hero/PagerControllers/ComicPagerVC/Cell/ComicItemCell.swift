//
//  ComicItemCell.swift
//  Marvel
//
//  Created by Pavel on 23.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit

class ComicItemCell: UITableViewCell {
    
    @IBOutlet weak var comicImageView: UIImageView!
    @IBOutlet weak var comicNameLabel: UILabel!
    
    var comic: ItemsCharactersType? {
        didSet {
            guard let name = comic?.name else { return }
            comicNameLabel.text = name
            
            guard let imageLink = comic?.resourceURI else { return }
            guard let url = URL(string: imageLink) else { return }
            comicImageView.kf.setImage(with: url)
        }
    }
    
    // QUESTION: зачем тут 2 метода, которые не переопределены
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nibInstance() -> UINib {
        let nib = UINib(nibName: String(describing: self), bundle: nil)
        return nib
    }
}
