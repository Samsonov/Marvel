//
//  DetailPagerVC.swift
//  Marvel
//
//  Created by Pavel on 24.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import XLPagerTabStrip
import CoreData

class DetailPagerVC: UIViewController, IndicatorInfoProvider {
    @IBOutlet weak var detailWebView: UIWebView!
    
    var webView:            WKWebView!
    var index:              Int32?
    var url:                String?
    var activityIndicator = ActivityIndicatorVC()
    let entityName        = "DetailPager"
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    init(index: Int32?) {
        self.index = index
        super.init(nibName: nil, bundle: nil)
    }
    
    init(url: String?, index: Int32?) {
        self.index = index
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if connectToInternet {
            activityIndicator.showActivityIndicator(view)
            
            if let url = self.url {
                ApiConnection.sharedInstance.getHTMLString(url: url, completionHandler: { [weak self] data in
                    guard let sself = self else { return }
                    sself.loadToCoreData(id: sself.index!, htmlString: data)
                })
                
                let request = URLRequest(url: URL(string: url)!)
                webView.load(request)
            }
        } else {
            activityIndicator.showActivityIndicator(view)
            do {
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
                let results = try context.fetch(request)
                
                // QUESTION: плохо, как можно переделать?
                if results.count > 0 {
                    for result in results {
                        if let indexPath = (result as! DetailPager).value(forKey: "id") as? Int32 {
                            if index! == indexPath {
                                let htmlString = (result as! DetailPager).value(forKey: "htmlString")
                                webView.loadHTMLString(htmlString as! String, baseURL: nil)
                                break
                            }
                        }
                    }
                }
            } catch {
                print("Error when requesting DetailPager")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Detail")
    }
}

extension DetailPagerVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.hideProgressView()
    }
}

// MARK: CoreData
extension DetailPagerVC {
    fileprivate func loadToCoreData(id: Int32, htmlString: String) -> Void {
        var existIndex = false
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results {
                    if let indexPath = (result as! DetailPager).value(forKey: "id") as? Int32 {
                        if index == indexPath {
                            existIndex = false
                            break
                        } else {
                            existIndex = true
                        }
                    }
                }
            } else {
                existIndex = true
            }
        } catch {
            print("Not found")
        }
        
        if existIndex {
            let detail = DetailPager(context: context)
            detail.id = id
            detail.htmlString = htmlString
        }
        do {
            try context.save()
        } catch {
            print("Failed to save")
        }
    }
}
