//
//  WikiPagerVC.swift
//  Marvel
//
//  Created by Pavel on 18.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import CoreData

class WikiPagerVC: UIViewController, IndicatorInfoProvider {
    let characterId:  Int32?
    let entityName    = "WikiPager"
    let descriptionNo = "No description"
    
    var dataCharacter     = [Character]()
    var activityIndicator = ActivityIndicatorVC()

    @IBOutlet weak var nameLabel:          UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var descriptionLabel:   UILabel!
    
    init(characterId: Int32) {
        self.characterId = characterId
        super.init(nibName: "WikiPagerVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = empty
        descriptionLabel.text = empty
        
        if connectToInternet {
            activityIndicator.showActivityIndicator(view)
            
            ApiConnection.sharedInstance.getCharactersWithId(characterId!, completionHandler: { [weak self] data in
                guard let sself = self else { return }
                
                // QUESTION а если от сервера прийдет пустой или невалдиный ответ? что будет?
                sself.dataCharacter = data!
                let character = sself.dataCharacter.first
                
                sself.nameLabel.text = character?.name
                
                if (character?.description != empty) {
                    sself.descriptionLabel.text = character?.description
                    sself.descriptionLabel.textAlignment = .left
                } else {
                    sself.descriptionLabel.text = sself.descriptionNo
                }
                
                if let imageURL = character?.thumbnail?.fullPathImage(ImageStandardSize.fantastic) {
                    sself.setLayerParameters(imageURL: imageURL)
                    
                    sself.loadToCoreData(index: Int32(sself.characterId!),
                                          name: (character?.name)!,
                                 thumbnailPath: imageURL,
                              descriptionComic: (character?.description)!)
                }
                
                DispatchQueue.main.async {
                    sself.activityIndicator.hideProgressView()
                }
            }, failure: { error in
                if let err = error {
                    print(err)
                }
            })
        } else {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            request.returnsObjectsAsFaults = false
            
            do {
                let results = try context.fetch(request)
                
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        
                        if Int32(characterId!) == (result.value(forKey: "index") as? Int32)! {
                            self.nameLabel.text = result.value(forKey: "name") as! String?
                            
                            // QUESTION: что и зачем делается в этом строке?
                            if let imageURL = result.value(forKey: "thumbnailPath") as! String? {
                                self.setLayerParameters(imageURL: imageURL)
                            }
                            
                            let description = result.value(forKey: "descriptionComic") as! String
                            
                            if description != empty {
                                self.descriptionLabel.text = description
                                self.descriptionLabel.textAlignment = .left
                            } else {
                                self.descriptionLabel.text = descriptionNo
                            }
                            break
                        } else {
                            self.nameLabel.text = descriptionNo
                        }
                    }
                }
            } catch {
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Portrait")
    }
}

extension WikiPagerVC {
    func setLayerParameters(imageURL: String) -> Void {
        self.thumbnailImageView.kf.setImage(with: URL(string: imageURL))
        self.thumbnailImageView.layer.borderColor   = UIColor.darkGray.cgColor
        self.thumbnailImageView.layer.borderWidth   = 1.0
        self.thumbnailImageView.layer.cornerRadius  = self.thumbnailImageView.bounds.width / 2.0
        self.thumbnailImageView.layer.masksToBounds = true
    }
}

// MARK: CoreData

extension WikiPagerVC {
    fileprivate func loadToCoreData(index: Int32, name: String, thumbnailPath: String, descriptionComic: String) -> Void {
        
        var existIndex = false
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                // Для поиска элементов в массиве есть специальные функции
                // например
//                results.index(where: { element -> Bool in
//                    condition
//                })
                for result in results as! [NSManagedObject] {
                    if let indexPath = result.value(forKey: "index") as? Int32 {
                        if index != indexPath {
                            existIndex = true
                        } else {
                            existIndex = false
                            break
                        }
                    }
                }
            } else {
                existIndex = true
            }
        } catch {
            print("Not found")
        }
        
        if existIndex {
            let newComic = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
            
            newComic.setValue(index, forKey: "index")
            newComic.setValue(name, forKey: "name")
            newComic.setValue(thumbnailPath, forKey: "thumbnailPath")
            newComic.setValue(descriptionComic, forKey: "descriptionComic")
            
            do {
                try context.save()
            } catch {
                print("Failed to save")
            }
        }
    }
}





















