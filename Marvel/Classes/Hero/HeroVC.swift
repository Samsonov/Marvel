//
//  HeroVC.swift
//  Marvel
//
//  Created by Pavel on 17.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class HeroVC: ButtonBarPagerTabStripViewController {
    var dataCharacter: Character?
    var index: Int32?
   
    override func viewDidLoad() {
        self.settings.style.buttonBarItemBackgroundColor = .darkGray
        self.settings.style.selectedBarHeight            = 2.0
        self.settings.style.selectedBarBackgroundColor   = .red
        
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    static func storyboardInstance() -> HeroVC {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HeroVC
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let detailPagerVC: DetailPagerVC
        let wikiPagerVC:   WikiPagerVC
        let comicPagerVC:  ComicPagerTVC
        
        if connectToInternet {
            detailPagerVC = DetailPagerVC(url: dataCharacter!.detailLink, index: index)
            wikiPagerVC   = WikiPagerVC(characterId: Int32((dataCharacter?.id)!))
            comicPagerVC  = ComicPagerTVC.storyboardInstance()
            comicPagerVC.characterId = Int32((dataCharacter?.id)!)
        } else {
            detailPagerVC = DetailPagerVC(index: index)
            wikiPagerVC   = WikiPagerVC(characterId: index!)
            comicPagerVC  = ComicPagerTVC.storyboardInstance()
            comicPagerVC.characterId = index
        }
        
        return [detailPagerVC, wikiPagerVC, comicPagerVC]
    }
}

class TestViewController: UIViewController, IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Test")
    }
}












