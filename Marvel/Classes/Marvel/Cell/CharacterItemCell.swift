//
//  CharacterItemCell.swift
//  Marvel
//
//  Created by Pavel on 11.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit

let offsetSpeed: CGFloat = 20.0

class CharacterItemCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setImageSize(size: CGSize) -> Void {
        thumbnailImageView.frame.size = size
    }

    func offset(_ offset: CGPoint)
    {
        thumbnailImageView.frame = self.thumbnailImageView.bounds.offsetBy(dx: offset.x, dy: offset.y)
    }
}



















