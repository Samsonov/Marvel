//
//  FooterCollectionReusableView.swift
//  Marvel
//
//  Created by Pavel on 01.02.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit

class FooterCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // QUESTION: что тут забыто?
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showActivityIndicator),
                                               name: listenerNetworkReachableNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(hideActivityIndicator),
                                               name: listenerNetworkNotReachableNotification,
                                               object: nil)
    }
    
    func showActivityIndicator() -> Void {
        self.activityIndicator.isHidden = false
    }
    
    func hideActivityIndicator() -> Void {
        self.activityIndicator.isHidden = true
    }
}
