//
//  MarvelCollectionVC.swift
//  Marvel
//
//  Created by Pavel Samsonov on 02.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit
import Kingfisher
import CoreData

private let characterReuseIdentifier = "CharacterCell"
private let footerReuseIdentifier    = "FooterViewCell"
let entityName                       = "MarvelCollection"
let parentEntity                     = "ParentClass"

var loadMoreStatus = true
var limit = 20
var offset = 0

class FooterCollectionView: UICollectionReusableView {
    @IBOutlet weak var viewFooter: UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.viewFooter = UIView(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MarvelCollectionVC: UICollectionViewController {
    var dataCharacter     = [Character]()
    var dataPath          = [MarvelSummary]()
    var footerView        = FooterCollectionReusableView()
    var activityIndicator = ActivityIndicatorVC()
    
    var refreshControl: UIRefreshControl!
    
    var sizeCell = CGSize()
    
    let apiManager = ApiConnection.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CharacterItemCell", bundle: nil)
        self.collectionView?.register(nib, forCellWithReuseIdentifier: characterReuseIdentifier)
        
        let footer = UINib(nibName: "FooterCollectionReusableView", bundle: nil)
        self.collectionView?.register(footer, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerReuseIdentifier)
        
        self.navigationItem.title = "Marvel"
        
        let rightButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(rightRefreshButton))
        self.navigationItem.rightBarButtonItem = rightButton
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(connectReachable),
                                               name: listenerNetworkReachableNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(connectNotReachable),
                                               name: listenerNetworkNotReachableNotification,
                                               object: nil)
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Загрузка...")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        collectionView?.addSubview(refreshControl)
        
        // Удалить все объекты из CoreData
//        removeAllObjects()
        
        // Распечатать все объекты из CoreData
//        printAllObjects()
        
        if connectToInternet {
            activityIndicator.showActivityIndicator(collectionView!)
            loadData()
        } else {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            request.returnsObjectsAsFaults = false
            request.fetchBatchSize = limit
            
            
            
            
            do {
                // QUESTION: лучше было бы так 
                // if let results = try context.fetch(request) as? [MarvelCollection], !results.isEmpty
                let results = try context.fetch(request)
                if results.count > 0 {
                    for result in results as! [MarvelCollection] {
                        
                        let index = (result.value(forKey: "index") as? Int32)!
                        let resourceURI = (result.value(forKey: "thumbnailPath") as? String)!
                        
                        let summary = MarvelSummary(index: index, resourceURI: resourceURI)
                        dataPath.append(summary)
                    }
                    self.collectionView?.reloadData()
                }
            } catch {
                
            }
            // QUESTION: зачем 2 reloadData
            self.collectionView?.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK:- UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if connectToInternet {
            return dataCharacter.count
        } else {
            return dataPath.count
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: characterReuseIdentifier, for: indexPath) as! CharacterItemCell
        
        let offsetY = ((collectionView.contentOffset.y - cell.frame.origin.y) / cell.frame.height) * offsetSpeed
        cell.setImageSize(size: sizeCell)
        cell.offset(CGPoint(x: 0.0, y: offsetY))
    
        if connectToInternet {
            let character = dataCharacter[indexPath.row]
            
            if let imageURL = character.thumbnail?.fullPathImage(ImagePortraitSize.uncanny) {
                cell.thumbnailImageView.kf.setImage(with: URL(string: imageURL))
                loadToCoreData(index: Int32(character.id!), imagePath: imageURL)
            }
        } else {
            let path = dataPath[indexPath.row]
            cell.thumbnailImageView.kf.setImage(with: URL(string: path.resourceURI))
        }

        return cell
    }
    
// MARK:- UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let heroVC = HeroVC.storyboardInstance()
        
        if connectToInternet {
            heroVC.dataCharacter = dataCharacter[indexPath.row]
            heroVC.index = Int32(dataCharacter[indexPath.row].id!)
        } else {
            heroVC.index = dataPath[indexPath.row].index
        }
        
        navigationController?.pushViewController(heroVC, animated: true)
    }

}

// MARK:- UICollectionViewDelegateFlowLayout

fileprivate let item: CGFloat        = 0.25
fileprivate let itemsPerRow: CGFloat = 2.0
fileprivate let sectionInsets        = UIEdgeInsets(top: item, left: item, bottom: item, right: item)

extension MarvelCollectionVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
             layout collectionViewLayout: UICollectionViewLayout,
                 sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthPerItem = ((view.frame.width / itemsPerRow) - item * itemsPerRow)
        let heightPerItem = widthPerItem + widthPerItem / itemsPerRow
        
        sizeCell = CGSize(width: widthPerItem + 30.0, height: heightPerItem + 30.0)
        
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
             layout collectionViewLayout: UICollectionViewLayout,
               insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
             layout collectionViewLayout: UICollectionViewLayout,
             minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.top
    }
    
    func collectionView(_ collectionView: UICollectionView,
             layout collectionViewLayout: UICollectionViewLayout,
             referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width,
                     height: collectionView.frame.height - (collectionView.frame.height - 80.0))
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            footerView = (collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                             withReuseIdentifier: footerReuseIdentifier,
                                                                             for: indexPath)) as! FooterCollectionReusableView
        }
        return footerView
    }
}

extension MarvelCollectionVC {
    fileprivate func loadData() -> Void {
        ApiConnection.sharedInstance.getCharacters(limit, offset: offset, completionHandler: { [weak self] data in
            guard let sself = self else { return }
            guard let character = data else { return }
            
            sself.dataCharacter = character
            
            DispatchQueue.main.async {
                if sself.refreshControl.isRefreshing {
                    sself.refreshControl.endRefreshing()
                }
                
                sself.activityIndicator.hideProgressView()
                sself.collectionView?.reloadData()
            }
        }, failure: { error in
            if let err = error {
                print(err)
            }
        })
    }
}

// MARK:- Parallax and pagination

//w = 159.5, h = 239.25

extension MarvelCollectionVC {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // parallax for images
        
        guard let collectionView = self.collectionView else {return}
        guard let visibleCells = collectionView.visibleCells as? [CharacterItemCell] else {return}
        
        for parallaxCell in visibleCells {
            let offsetY = ((collectionView.contentOffset.y - parallaxCell.frame.origin.y) / parallaxCell.frame.height) * offsetSpeed
            parallaxCell.offset(CGPoint(x: 0.0, y: offsetY))
        }
        
      // pagination for images
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height < collectionView.frame.size.height ?
                                                            collectionView.frame.size.height :
                                                            scrollView.contentSize.height
        let maxHeight = contentHeight - (collectionView.frame.size.height)
        loadMoreStatus = offsetY < maxHeight ? false : loadMoreStatus
        
        if offsetY > maxHeight && !loadMoreStatus {
            loadMoreStatus = true
            footerView.isHidden = false
            footerView.activityIndicator.startAnimating()
            
            loadCharacters()
            
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(loadCharacters),
                                                   name: listenerNetworkReachableNotification,
                                                   object: nil)
        }
    }
}

// MARK:- ReachableInternet

extension MarvelCollectionVC {
    func loadCharacters() -> Void {
        var indexPaths = [IndexPath]()
        let offset     = collectionView?.numberOfItems(inSection: 0)
        
        if connectToInternet {
            ApiConnection.sharedInstance.getCharacters(limit, offset: offset!, completionHandler: { [weak self] data in
                guard let sself = self else { return }
                guard let character = data else { return }
                
                character.forEach({ (hero) in
                    sself.dataCharacter.append(hero)
                    indexPaths.append(IndexPath.init(item: sself.dataCharacter.endIndex - 1, section: 0))
                })
                
                if sself.footerView.activityIndicator.isAnimating {
                    sself.footerView.activityIndicator.stopAnimating()
                }
                
                DispatchQueue.main.async {
                    sself.collectionView?.insertItems(at: indexPaths)
                }
                }, failure: { error in
                    if let err = error {
                        print(err)
                    }
            })
        } else {
            self.footerView.activityIndicator.isHidden = true
        }
    }
    
    func connectReachable() -> Void {
        let statusBar = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = .clear
        }
    }
    
    func connectNotReachable() -> Void {
        let statusBar = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = .red
        }
    }
    
    func rightRefreshButton() -> Void {
        if connectToInternet {
            loadData()
            collectionView?.reloadData()
        }
    }
}

// MARK:- CoreData

extension MarvelCollectionVC {
    fileprivate func loadToCoreData(index: Int32, imagePath: String) -> Void {
        var existIndex = true
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        if let results = try? context.fetch(request) as? [MarvelCollection], !results.isEmpty {
            existIndex = !results.contains({$0.index == index})
            
            
//            if let index = results?.index({$0.index == index}) {
//                existIndex = false
//            }
        }
        
//            for result in results as! [MarvelCollection] {
//                if let indexPath = result.value(forKey: "index") as? Int32 {
//                    if index != indexPath {
//                        existIndex = true
//                    } else {
//                        existIndex = false
//                        break
//                    }
//                }
//            }
//        }
        
//            if results.count > 0 {
//                for result in results as! [MarvelCollection] {
//                    if let indexPath = result.value(forKey: "index") as? Int32 {
//                        if index != indexPath {
//                            existIndex = true
//                        } else {
//                            existIndex = false
//                            break
//                        }
//                    }
//                }
//            } else {
//                existIndex = true
//            }
        
        if existIndex {
            let newImage = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
            
            newImage.setValue(index, forKey: "index")
            newImage.setValue(imagePath, forKey: "thumbnailPath")
    
            do {
                try context.save()
            } catch {
                print("Failed to save")
            }
        }
    }
    
    fileprivate func printAllObjects() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: parentEntity)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results as! [ParentClass] {
                    let index = result.value(forKey: "index")
                    let imagePath = result.value(forKey: "thumbnailPath")
                    
                    print(index!, imagePath!)
                }
            }
        } catch {
            print("Failed to print")
        }
    }
    
    fileprivate func removeAllObjects() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: parentEntity)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results as! [ParentClass] {
                    context.delete(result)
                }
                try context.save()
                print("Remove to saccess")
            }
        } catch {
            print("Failed to remove")
        }
    }
}

// MARK:- Refresh

extension MarvelCollectionVC {
    @objc fileprivate func refresh(sender:AnyObject) {
        loadData()
    }
}























