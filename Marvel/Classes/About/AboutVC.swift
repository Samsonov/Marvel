//
//  AboutVC.swift
//  Marvel
//
//  Created by Pavel Samsonov on 02.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var copyrightLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title  = "About"
        self.descriptionLabel.text = "Приложение Marvel Comics - все супергерои, злодеи и прочие персонажи собраны тут. Остается только найти интересующего героя Marvel в каталог, перейти на его страницу и узнать все о нем. Marvel Comics - американский издатель и бесспорный лидер на рынке комиксов."
        self.versionLabel.text     = "v1.0.0"
        self.copyrightLabel.text   = "©growapp.biz, 2017"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
