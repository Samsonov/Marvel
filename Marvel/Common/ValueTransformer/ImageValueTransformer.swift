//
//  ImageValueTransformer.swift
//  Marvel
//
//  Created by Pavel on 25.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit

class ImageValueTransformer: ValueTransformer {
    func allowsReverseTransformation() -> Bool {
        return true
    }
    
    func transformedValueClass() -> Swift.AnyClass {
        return NSData.self
    }
    
    override func transformedValue(_ value: Any?) -> Any? {
        let imageData = UIImageJPEGRepresentation(value as! UIImage, 1.0)
        return imageData
    }
    
    open override func reverseTransformedValue(_ value: Any?) -> Any? {
        let image = UIImage.init(data: value as! Data)
        return image
    }
}
