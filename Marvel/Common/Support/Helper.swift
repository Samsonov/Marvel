//
//  Helper.swift
//  Marvel
//
//  Created by Pavel Samsonov on 04.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit

let empty                = ""
let connectToInternet    = ApiConnection.sharedInstance.connectedToInternet()
let context              = CoreDataManager.sharedInstance.persistentContainer.viewContext

let listenerNetworkReachableNotification    = NSNotification.Name(rawValue: "NSNotificationKeyListenerNetworkReachableNotification")
let listenerNetworkNotReachableNotification = NSNotification.Name(rawValue: "NSNotificationKeyListenerNetworkNotReachableNotification")

class Helper: NSObject {}
