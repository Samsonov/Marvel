//
//  ApiConnection.swift
//  Marvel
//
//  Created by Pavel Samsonov on 04.01.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ApiConnection: NSObject {
    let sessionManager: Alamofire.SessionManager
    
    override init() {
        let configuration = URLSessionConfiguration.default
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    fileprivate lazy var publicKey: String = {
        let path = Bundle.main.path(forResource: "APIKeys", ofType: "plist")
        let key = NSDictionary(contentsOfFile: path!)
        return key!.object(forKey: "PUBLIC_API_KEY") as! String
    }()
    
    fileprivate lazy var privateKey: String = {
        let path = Bundle.main.path(forResource: "APIKeys", ofType: "plist")
        let key = NSDictionary(contentsOfFile: path!)
        return key!.object(forKey: "PRIVATE_API_KEY") as! String
    }()
    
    fileprivate lazy var getCharactersURL: String = {
        return self.urlForRequest("GET_CHARACTERS")
    }()
}

extension ApiConnection {
    public class var sharedInstance: ApiConnection {
        struct Singleton {
            static let instance : ApiConnection = ApiConnection()
        }
        return Singleton.instance
    }
}

extension ApiConnection {
    fileprivate func urlForRequest(_ requestKey: String) -> String {
        let path = Bundle.main.path(forResource: "URLs", ofType: "plist")
        let url = NSDictionary(contentsOfFile: path!)
        
        let baseURL = url?.object(forKey: "BASE_URL") as! String
        let relativeURL = url?.object(forKey: requestKey) as! String
        
        return baseURL + relativeURL
    }
    
    fileprivate func md5Hash(_ string: String) -> String {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        
        if let d = string.data(using: String.Encoding.utf8) {
            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                CC_MD5(body, CC_LONG(d.count), &digest)
            }
        }
        
        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }
}

extension ApiConnection {
    public func getCharacters(_ limit: Int, offset: Int, completionHandler:@escaping (_ characters: [Character]?) -> Void,
                                                                 failure:@escaping (_ error : Error?) -> Void) {
        let finalURL = String(format: getCharactersURL)
        let ts = Date().timeIntervalSince1970.description
        let hash = md5Hash("\(ts)\(privateKey)\(publicKey)")
        
        let parameters = ["apikey" : publicKey,
                          "limit"  : limit,
                          "offset" : offset,
                          "ts"     : ts,
                          "hash"   : hash] as [String : Any]
        
        sessionManager.request(finalURL, parameters: parameters)
            .responseObject { (response: DataResponse<DataWrapper<DataContainer<Character>>>?) in
                if let characters = response?.result.value?.data?.results {
                    completionHandler(characters)
                } else {
                    failure(response?.result.error)
                }
        }
    }
    
    public func getCharactersWithId(_ characterId: Int32, completionHandler:@escaping (_ characters: [Character]?) -> Void,
                                                                failure:@escaping (_ error : Error?) -> Void) {
        let finalURL = String(format: getCharactersURL) + "/" + String(characterId)
        let ts = Date().timeIntervalSince1970.description
        let hash = md5Hash("\(ts)\(privateKey)\(publicKey)")
        
        let parameters = ["apikey" : publicKey,
                          "ts"     : ts,
                          "hash"   : hash] as [String : Any]
        
        sessionManager.request(finalURL, parameters: parameters)
            .responseObject { (response: DataResponse<DataWrapper<DataContainer<Character>>>?) in
                if let characters = response?.result.value?.data?.results {
                    completionHandler(characters)
                } else {
                    failure(response?.result.error)
                }
        }
    }
    
    public func getSeriesWithId(characterId: Int32, completionHandler:@escaping (_ characters: [Series]?) -> Void,
                                                            failure:@escaping (_ error : Error?) -> Void) {
        let finalURL = String(format: getCharactersURL) + "/" + String(characterId) + "/" + "series"
        let ts = Date().timeIntervalSince1970.description
        let hash = md5Hash("\(ts)\(privateKey)\(publicKey)")
        
        let parameters = ["apikey" : publicKey,
                          "ts"     : ts,
                          "hash"   : hash] as [String : Any]
        
        sessionManager.request(finalURL, parameters: parameters)
            .responseObject { (response: DataResponse<DataWrapper<DataContainer<Series>>>?) in
                if let series = response?.result.value?.data?.results {
                    completionHandler(series)
                } else {
                    failure(response?.result.error)
                }
        }
    }
    
    public func getSeriesWithURL(url: String, completionHandler:@escaping (_ thumbnail: ImagesResource?) -> Void) {
        let ts = Date().timeIntervalSince1970.description
        let hash = md5Hash("\(ts)\(privateKey)\(publicKey)")
        
        let parameters = ["apikey" : publicKey,
                          "ts"     : ts,
                          "hash"   : hash] as [String : Any]
        
        sessionManager.request(url, parameters: parameters).responseObject { (response: DataResponse<Images>?) in
                if let image = response?.result.value?.data?.results?.first?.thumbnail {
                    completionHandler(image)
                } 
        }
    }
    
    public func getHTMLString(url: String, completionHandler:@escaping (_ htmlString: String) -> Void) {
        sessionManager.request(url).responseString { response in
            if let loadHTML = response.result.value {
                completionHandler(loadHTML)
            }
        }
    }
}

extension ApiConnection {
    public func connectedToInternet() -> Bool {
        let manager = NetworkReachabilityManager()
        
        return (manager?.isReachable)!
    }
}


























