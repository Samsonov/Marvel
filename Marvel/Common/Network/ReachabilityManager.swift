//
//  ReachabilityManager.swift
//  Marvel
//
//  Created by Pavel on 03.02.17.
//  Copyright © 2017 Pavel Samsonov. All rights reserved.
//

import Foundation
import Alamofire

enum ReachabilityManagerError: Error {
    case notReachable
}

class ReachabilityManager {
    let networkManager = NetworkReachabilityManager()!
    
    static let shared: ReachabilityManager = ReachabilityManager()
    static var shared: ReachabilityManager = {
        return ReachabilityManager()
    }()
    
    public class var sharedInstance: ReachabilityManager {
        struct Singleton {
            static let instance : ReachabilityManager = ReachabilityManager()
        }
        return Singleton.instance
    }

    func checkInternetConnection() -> Bool {
        switch networkManager.networkReachabilityStatus {
        case NetworkReachabilityManager.NetworkReachabilityStatus.notReachable: return false
        case NetworkReachabilityManager.NetworkReachabilityStatus.unknown: return false
            
        default:
            return true
        }
    }
    
    //MARK:-
    //MARK: listening
    
    func startNetworkListening() {
        networkManager.listener = { status in
            switch status {
            case .reachable   : NotificationCenter.default.post(Notification(name: listenerNetworkReachableNotification))
            case .notReachable: NotificationCenter.default.post(Notification(name: listenerNetworkNotReachableNotification))
            default: break
            }
        }
            
        self.networkManager.startListening()
    }
    
    func stopNetworkListening() {
        networkManager.stopListening()
    }
}











